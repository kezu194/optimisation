from typing import List, Tuple

import numpy as np
from matplotlib import pyplot as plt


def jacobien(points: List[List[int]]) -> Tuple[List[Tuple[int, int]], List[int]]:
    """
    Create the data needed for the creation of a Jacobin Matrix.
    This function also return the value of f(x) because it will be used later.
    @param points: List of points as (x, y)
    @return:
    """
    abscisses = []
    ordonnes = []
    for x, y in points:
        abscisses.append((- x, -1))
        ordonnes.append(y)
    return abscisses, ordonnes


def moindre_carre(points: List[List[int]]) -> np.matrix:
    """
    Calculate the minimum value of x, y with the moindre carre algorithm from the given list of points.

    @param points: the list of points where our function go threw
    @return: The Jacobien matrix
    """

    abscisse, ordonne = jacobien(points=points)
    matrice = np.matrix(abscisse)
    ordonne = np.matrix(ordonne)
    result = np.matmul(matrice.getT(), matrice)
    result = result.getI() * -1
    result = np.dot(result, matrice.getT())

    return np.round(np.dot(result, ordonne.getT()), 2)


def function(x: np.ndarray, determined_values: np.matrix):
    return determined_values[0][0] * x + determined_values[1][0]


def moindre_carre_with_bruit(points):
    bruits = np.random.rand(len(points), 1)
    for i in range(len(points)):
        points[i][1] += bruits[i][0]
    return moindre_carre(points)


list_points = [[2, 2], [4, 4.3], [5, 4.9]]
print(jacobien(list_points))
print(moindre_carre(list_points))


values = moindre_carre(list_points)
x: np.ndarray = np.linspace(start=0, stop=6)
plt.plot(x, function(x, values))
for point in list_points:
    plt.plot(point[0], point[1], marker="o", markeredgecolor="blue", markerfacecolor="black")
plt.legend(["Fonction déterminé", "Point de passage de la fonction"])
plt.show()
