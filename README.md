# Optimisation

**Ce document regroupera les résponses aux exercices sur la méthode du gradient et dur moindre carré**
Pour visualiser les réponses dans les meilleures conditions, interprété ce document comme un Markdown sur votre système ou via le visualiseur de Gitlab : https://gitlab.com/kezu194/optimisation


Note : Si vous souhaitez exécuter le code de ce programme, utiliser la commande suivante pour installer tous les paquets python nécessaire :
```commandline
pip install -r requirements.txt
```

## 2. Recherche d’un optimum par la méthode du Gradient

#### 1) calculer à la main la dérivée de chaque fonction. Tracer la fonction et sa dérivée (≡sa pente), et exécuter la méthode du gradient pour trouver un minimum. Prendre par exemple :

| Name      | Quantity           |
|-----------|--------------------|
| α = 0.01  | x0 = 0             |
| ε = 10^-5 | nb max iter = 1000 |

Nous avons ainsi les fonctions et leurs dérivés suivants (en écriture python, '**2' équivaut à ²) :
- f(x) = (x - 2)**2
- f'(x) = 2 * (x - 2)
- g(x) = -exp(-(x-1)**2) * (x - 2)**2
- g'(x) = 2 * (x - 2) * (x**2 - 3 * x + 1) * exp(-(x-1)**2)

Dans le code suivant, nous définissons les fonctions f(x) et g(x) dans des fonctions python ainsi que les fonctions f'(x) et g'(x).
Ensuite, nous utilisons pyplot de matplotlib ainsi que du paquet numpy pour générer les graphiques de nos courbes.

```doctest
from typing import Callable, Tuple, List
import numpy as np
from numpy import exp
from matplotlib import pyplot as plt


def f(x: float) -> float:
    """
    Return the value of the f(x) function established.

    :param x: the value of x
    :return: the result of f(x) with the given x
    """
    return (x - 2) ** 2


def df(x: float) -> float:
    """
    Return the derived value of the f(x) function.

    :param x: the value of x
    :return: the result of f'(x) with the given x
    """
    return 2 * (x - 2)


def g(x: float) -> float:
    """
    Return the value of the g(x) function established.

    :param x: the value of x
    :return: the result of g(x) with the given x
    """
    return -exp(-(x - 1) ** 2) * (x - 2) ** 2


def dg(x: float) -> float:
    """
    Return the derived value of the g(x) function.

    :param x: the value of x
    :return: the result of g'(x) with the given x
    """
    return 2 * (x - 2) * (x**2 - 3 * x + 1) * exp(-(x-1)**2)
    
# We display the graph of f(x) in red and f'(x) in blue
x = np.linspace(start=-1, stop=3)
plt.plot(x, f(x), color="red")
plt.plot(x, df(x), color="blue")
plt.legend(["f(x)", "f'(x)"])
plt.grid()
plt.show()

# We display the graph of g(x) in red and g'(x) in blue
x = np.linspace(start=-2.5, stop=2.5)
plt.plot(x, g(x), color="red")
plt.plot(x, dg(x), color="blue")
plt.legend(["g(x)", "g'(x)"])
plt.grid()
plt.show()
```

Ainsi ce code génèrera les images suivantes :
![](gradient/img/courbe_f.png "Courbe f(x)")
![](gradient/img/courbe_g.png "Courbe g(x)")

Nous utilisons ensuite une nouvelle fonction pour récupérer le minimum local des fonctions à partir de x=0 grâce à la descente de gradient :
```doctest
def gradient(fx: Callable, dfx: Callable, xn: float = 0, precision_required: float = 10 ** -5,
             max_iteration: int = 1000, step: float = 0.01) -> Tuple[float, float, int]:
    """
    Calculate the min value of a function with the gradient algorithm.

    :param fx: function f(x)
    :param dfx: the derivative of the function
    :param xn: the initial point of the function (x value)
    :param precision_required: the precision required to stop the algorithm (ex: 10**-5)
    :param max_iteration: max number of iteration before stopping the algorithm. default = 1000
    :param step: the step after each iteration. default = 0.01
    :return: A tuple containing the value of x, the result of the final f(x) and the number of iteration
    """
    derive: float = dfx(xn)
    iteration: int = 0
    # If the absolute value of the derivative is inferior or equal to our required precision (epsilon) or if we reach
    # the max iteration we stop the program
    while abs(derive) > precision_required and iteration < max_iteration:
        xn = xn - (step * derive)
        derive = dfx(xn)
        iteration += 1
    return xn, fx(xn), iteration
    
# Gradient pour f(x)
print(gradient(fx=f, dfx=df, xn=0))
# Gradient pour g(x)
print(gradient(fx=g, dfx=dg, xn=0))
```

Ce programme retournera ainsi les valeurs suivantes :
```doctest
# xmin, f(xmin), nb iteration
(1.9999950512859124, 2.4489771121077752e-11, 639)
(0.3819640510202257, -1.786855978404731, 244)
```

#### 2) Modifier l’algorithme précédent afin de conserver en mémoire toutes les valeurs prises par x et f (x) lors des itérations. Programmer une fonction Python qui commence par lancer la méthode du gradient puis trace dans une fenêtre graphique le chemin pris par l’algorithme lors de la minimisation (avec la fonction plot). Cette fonction a exactement les mêmes arguments que dans la question précédente.

On modifie la fonction "gradient" précédente pour retourner en plus, une liste des coordonnées de chaque point parcouru par la descente de gradient :
```doctest
def gradient_with_list_points(fx: Callable, dfx: Callable, xn: float = 0, precision_required: float = 10 ** -5,
                              max_iteration: int = 1000, step: float = 0.01) -> (
        Tuple)[float, float, int, List[List[float]]]:
    """
    Same function as gradient(...) but also return a list of all points the algorithm pass threw.

    :param fx: function f(x)
    :param dfx: the derivative of the function
    :param xn: the initial point of the function (x value)
    :param precision_required: the precision required to stop the algorithm (ex: 10**-5)
    :param max_iteration: max number of iteration before stopping the algorithm. default = 1000
    :param step: the step after each iteration. default = 0.01
    :return: A tuple containing the value of x, the result of the final f(x), the number of iteration and the list of points
    """
    derive: float = dfx(xn)
    iteration: int = 0
    list_points: List[List[float]] = [[xn, fx(xn)]]
    # If the absolute value of the derivative is inferior or equal to our required precision (epsilon) or if we reach
    # the max iteration we stop the program
    while abs(derive) > precision_required and iteration < max_iteration:
        derive = dfx(xn)
        xn = xn - (step * derive)
        iteration += 1
        list_points.append([xn, fx(xn)])
    return xn, fx(xn), iteration, list_points
```

Dans un second temps, nous réalisons un petit programme pour prendre la nouvelle liste de points et les afficher dans un graphique avec la représentation graphique de la fonction concernée:
```doctest
# Gradient pour f(x)
x = np.linspace(start=0, stop=4)
_, _, _, result = gradient_with_list_points(fx=f, dfx=df, xn=0)
plt.plot(x, f(x), color="red")
for elem in result:
    plt.plot(elem[0], elem[1], marker="o", markeredgecolor="blue", markerfacecolor="black")
plt.grid()
plt.legend(["f(x)", "Avancement de la descente de gradient"])
plt.show()

# Gradient pour g(x)
x = np.linspace(start=-0.1, stop=1.2)
_, _, _, result = gradient_with_list_points(fx=g, dfx=dg, xn=0)
plt.plot(x, g(x), color="red")
for elem in result:
    plt.plot(elem[0], elem[1], marker="o", markeredgecolor="blue", markerfacecolor="black")
plt.grid()
plt.legend(["g(x)", "Avancement de la descente de gradient"])
plt.show()
```
Ce programme nous génèrera ainsi les images suivantes :

![](gradient/img/gradient_f_left.png "Courbe f(x) avec descente de gradient")
![](gradient/img/gradient_g_left.png "Courbe g(x) avec descente de gradient")

#### 3) Comparer les résultats obtenus pour différentes valeurs du point initial (x0 = 0, x0 = 5)

Nous venons de tester dans la question précédente, la descente de gradient avec comme valeur initial pour les deux fonctions, x0 = 0.
Avec notre configuration actuelle de notre descente de gradient, nous obtenons les images suivantes pour une valeur de x0 fixé à 5 :

![](gradient/img/gradient_f_right.png "Courbe f(x) avec descente de gradient")
![](gradient/img/gradient_g_right.png "Courbe g(x) avec descente de gradient")

Nous remarquons que pour la fonction g(x), le programme s'arrête après un seul point, car nous avons trouvé un minimum local. En effet, la valeur de g'(5) est assez proche de 0 pour être en dessous de notre précision souhaitée (epsilon) et ainsi stopper le programme.

#### 4) Comparer les résultats obtenus pour différentes valeurs de α. Que voit-on graphiquement ?

Nous considérons a comme le pas d'avancement après chaque itération du programme de la descente de gradient. Nous réessayons le programme précédent avec un pas bien supérieur (0.1) que celui de base (0.01) :
```doctest
# Gradient pour f(x)
x = np.linspace(start=0, stop=5.5)
_, _, _, result = gradient_with_list_points(fx=f, dfx=df, xn=0, step=0.1)
plt.plot(x, f(x), color="red")
for elem in result:
    plt.plot(elem[0], elem[1], marker="o", markeredgecolor="blue", markerfacecolor="black")
plt.grid()
plt.legend(["f(x)", "Avancement de la descente de gradient"])
plt.show()

# Gradient pour g(x)
x = np.linspace(start=-0.1, stop=5.5)
_, _, _, result = gradient_with_list_points(fx=g, dfx=dg, xn=0, step=0.1)
plt.plot(x, g(x), color="red")
for elem in result:
    plt.plot(elem[0], elem[1], marker="o", markeredgecolor="blue", markerfacecolor="black")
plt.grid()
plt.legend(["g(x)", "Avancement de la descente de gradient"])
plt.show()
```
Ce programme génère les images suivantes :

![](gradient/img/f_gradient_big_step.png "Courbe f(x) avec descente de gradient")
![](gradient/img/g_gradient_big_step.png "Courbe g(x) avec descente de gradient")

Nous remarquons que la descente de gradient se réalise beaucoup plus rapidement mais est aussi un peu moin précise en la recherche de minimum.

#### 5) Programmer une méthode avec “inertie” : α n’est désormais plus une constante, mais évoluant entre αmin et αmax

Pour réaliser cette inertie, nous allons modifier la dernière fonction de la descente de gradient :
```doctest
def gradient_inertie(fx: Callable, dfx: Callable, xn: float = 0, precision_required: float = 10 ** -5,
                     max_iteration: int = 1000, step: float = 0.01, step_min: float = 0.001, step_max=0.1) -> (
        Tuple)[float, float, int, List[List[float]]]:
    """
    Same function as gradient(...) but also return a list of all points the algorithm pass threw.

    :param fx: function f(x)
    :param dfx: the derivative of the function
    :param xn: the initial point of the function (x value)
    :param precision_required: the precision required to stop the algorithm (ex: 10**-5)
    :param max_iteration: max number of iteration before stopping the algorithm. default = 1000
    :param step: the step after each iteration. default = 0.01
    :param step_min: the min step after each iteration. default = 0.01
    :param step_max: the max step after each iteration. default = 0.1
    :return: A tuple containing the value of x, the result of the final f(x), the number of iteration and the list of points
    """

    def update_step(derive_xn: float, xn_plus_1: float, step: float) -> float:
        """
        Update the step (alpha) of the gradient function, based on the max and min value defined for the step.

        @param derive_xn: the derivative value of xn
        @param xn_plus_1: the xn+1 value
        @param step: the actual step
        @return: the new step
        """
        derive_xn_plus_1: float = dfx(xn_plus_1)
        signe_xn = 0 if derive_xn < 0 else 1
        signe_xn_plus_1 = 0 if derive_xn_plus_1 < 0 else 1
        if signe_xn == signe_xn_plus_1:
            step *= 1.5
            if step > step_max:
                step = step_max
        else:
            step = step / 2
            if step < step_min:
                step = step_min
        return step

    derive: float = dfx(xn)
    iteration: int = 0
    list_points: List[List[float]] = [[xn, fx(xn)]]
    # If the absolute value of the derivative is inferior or equal to our required precision (epsilon) or if we reach
    # the max iteration we stop the program
    while abs(derive) > precision_required and iteration < max_iteration:
        derive = dfx(xn)
        xn = xn - (step * derive)
        step = update_step(derive_xn=derive, xn_plus_1=xn, step=step)
        iteration += 1
        list_points.append([xn, fx(xn)])
    return xn, fx(xn), iteration, list_points


# Gradient pour f(x)
x = np.linspace(start=0, stop=5.5)
_, _, _, result = gradient_inertie(fx=f, dfx=df, xn=5)
plt.plot(x, f(x), color="red")
for elem in result:
    plt.plot(elem[0], elem[1], marker="o", markeredgecolor="blue", markerfacecolor="black")
plt.grid()
plt.legend(["f(x)", "Avancement de la descente de gradient"])
plt.show()

# Gradient pour g(x)
x = np.linspace(start=-0.1, stop=5.5)
_, _, _, result = gradient_inertie(fx=g, dfx=dg, xn=1.5)
plt.plot(x, g(x), color="red")
for elem in result:
    plt.plot(elem[0], elem[1], marker="o", markeredgecolor="blue", markerfacecolor="black")
plt.grid()
plt.legend(["g(x)", "Avancement de la descente de gradient"])
plt.show()
```

Dans cette nouvelle version, nous introduisons une nouvelle fonction **update_step**. Cette fonction va vérifier le signe des dérivés de f'(xn) et f'(xn+1).
Ainsi si les signes sont identiques, nous augmentons la valeur du step en nous assurant qu'elle ne dépasse pas notre step_max établit (0.1).
Si les signes sont différents, nous diminuons la valeur du step en nous assurant qu'elle soit toujours supérieur à notre step_min établit (0.001).

Nous obtenons avec ce programme des courbes comme ceci :

![](gradient/img/f_inertie.png "Courbe f(x) avec descente de gradient")
![](gradient/img/g_inertie.png "Courbe g(x) avec descente de gradient")

Nous remarquons que les étapes de notre descente de gradient sont plus ou moin écarté sur la courbe. Les étapes commencent généralement avec un léger écart, puis plus la courbe est pentu, plus l'écart augmente. Enfin lorsque nous nous rapprochons de la valeur du minimum local, l'écart se resserre de nouveau.


## 3. Moindres carrés pour l’approximation linéaire de données

#### On considère que les points d’observations (ti, vi) sont représentés comme deux vecteurs N ×1 t et v. Écrire une fonction qui calcule le Jacobien à partir de t. On peut commencer par tester avec les points : (2, 2); (4, 4.3); et (5, 4.9).

On écrit une fonction afin de récupérer la matrice jacobienne à partir des points donnés en argument. À noter que nous récupérons aussi la liste des valeurs de y qui nous serons utile plus tard :
```doctest
def jacobien(points: List[List[int]]) -> Tuple[List[Tuple[int, int]], List[int]]:
    """
    Create the data needed for the creation of a Jacobien Matrix.
    This function also return the value of f(x) because it will be used later.
    @param points: List of points as (x, y)
    @return:
    """
    abscisses = []
    ordonnes = []
    for x, y in points:
        abscisses.append([- x, -1])
        ordonnes.append(y)
    return abscisses, ordonnes
```

Ainsi en exécutant cette fonction avec les points donnés en exemple, nous obtenons le résultat suivant :

```doctest
list_points = [[2, 2], [4, 4.3], [5, 4.9]]
print(jacobien(points=list_points))
```
Result:
```commandline
([(-2, -1), (-4, -1), (-5, -1)], [2, 4.3, 4.9])
```

#### Écrire une fonction qui calcule le minimum de f (a, b) en fonction de t et v. On réalise ainsi une approximation linéaire des points d’observations. Tester avec les 3 points précédents. Tracer les points d’observations et la droite calculée sur le même graphique

Nous rédigeons une fonction nous permettant de trouver les minimums de x et y grâce aux points donnés en argument :
```doctest
def moindre_carre(points: List[List[int]]) -> np.matrix:
    """
    Calculate the minimum value of x, y with the moindre carre algorithm from the given list of points.

    @param points: the list of points where our function go threw
    @return: The Jacobien matrix
    """

    abscisse, ordonne = jacobien(points=points)
    matrice = np.matrix(abscisse)
    ordonne = np.matrix(ordonne)
    result = np.matmul(matrice.getT(), matrice)
    result = result.getI() * -1
    result = np.dot(result, matrice.getT())

    return np.round(np.dot(result, ordonne.getT()), 2)
```

Cette fonction executé de la même façon que notre dernière fonction retournera ceci :

```commandline
[[0.99]
 [0.09]]
```

Nous pouvons ainsi représenté notre résultat dans un graphique avec nos points données ainsi que la fonciton qui se dessine :

```doctest
def function(x: np.ndarray, determined_values: np.matrix):
    return determined_values[0][0] * x + determined_values[1][0]


values = moindre_carre(list_points)
x: np.ndarray = np.linspace(start=0, stop=6)
plt.plot(x, function(x, values))
for point in list_points:
    plt.plot(point[0], point[1], marker="o", markeredgecolor="blue", markerfacecolor="black")
plt.legend(["Fonction déterminé", "Point de passage de la fonction"])
plt.show()
```

![](moindre_carre/img/function.png "Courbe f(x) avec descente de gradient")

Nous remarquons que la droite ne passe pas exactement par les points mais qui se rapproche de la véritable fonction.

#### Pour mieux tester notre routine, on va effectuer une “génération simulée de données”: On considère par exemple la droite y = 2x + 1. Écrire une fonction qui génère N points sur cette droite (donc un vecteur t et v de taille N × 1 chacun) et ajouter du “bruit aléatoire” (avec la fonction rand(N, 1)) Tester votre approximation linéaire.

Pour réaliser ce test, nous allons créer une fonction qui va s'exécuter avant la génération de la matrix jacobienne afin de modifier les points f(x) avec un nombre aléatoire.

```doctest
def moindre_carre_with_bruit(points):
    bruits = np.random.rand(len(points), 1)
    for i in range(len(points)):
        points[i][1] += bruits[i][0]
    return moindre_carre(points)
```

Ainsi cette fonction permet de tester différents scénarios de points d'entrés avec ce bruit aléatoire.

![](moindre_carre/img/funciton_with_bruit.png "Courbe f(x) avec descente de gradient")
