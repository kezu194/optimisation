from typing import Callable, Tuple, List
import numpy as np
from numpy import exp
from matplotlib import pyplot as plt


def f(x: float) -> float:
    """
    Return the value of the f(x) function established.

    :param x: the value of x
    :return: the result of f(x) with the given x
    """
    return (x - 2) ** 2


def df(x: float) -> float:
    """
    Return the derived value of the f(x) function.

    :param x: the value of x
    :return: the result of f'(x) with the given x
    """
    return 2 * (x - 2)


def g(x: float) -> float:
    """
    Return the value of the g(x) function established.

    :param x: the value of x
    :return: the result of g(x) with the given x
    """
    return -exp(-(x - 1) ** 2) * (x - 2) ** 2


def dg(x: float) -> float:
    """
    Return the derived value of the g(x) function.

    :param x: the value of x
    :return: the result of g'(x) with the given x
    """
    return 2 * (x - 2) * (x ** 2 - 3 * x + 1) * exp(-(x - 1) ** 2)


def gradient(fx: Callable, dfx: Callable, xn: float = 0, precision_required: float = 10 ** -5,
             max_iteration: int = 1000, step: float = 0.01) -> Tuple[float, float, int]:
    """
    Calculate the min value of a function with the gradient algorithm.

    :param fx: function f(x)
    :param dfx: the derivative of the function
    :param xn: the initial point of the function (x value)
    :param precision_required: the precision required to stop the algorithm (ex: 10**-5)
    :param max_iteration: max number of iteration before stopping the algorithm. default = 1000
    :param step: the step after each iteration. default = 0.01
    :return: A tuple containing the value of x, the result of the final f(x) and the number of iteration
    """
    derive: float = dfx(xn)
    iteration: int = 0
    # If the absolute value of the derivative is inferior or equal to our required precision (epsilon) or if we reach
    # the max iteration we stop the program
    while abs(derive) > precision_required and iteration < max_iteration:
        xn = xn - (step * derive)
        derive = dfx(xn)
        iteration += 1
    return xn, fx(xn), iteration


# We display the graph of f(x) in red and f'(x) in blue
x = np.linspace(start=-1, stop=3)
plt.plot(x, f(x), color="red")
plt.plot(x, df(x), color="blue")
plt.legend(["f(x)", "f'(x)"])
plt.grid()
plt.show()

# We display the graph of g(x) in red and g'(x) in blue
x = np.linspace(start=-2.5, stop=2.5)
plt.plot(x, g(x), color="red")
plt.plot(x, dg(x), color="blue")
plt.legend(["g(x)", "g'(x)"])
plt.grid()
plt.show()

# Gradient pour f(x)
print(gradient(fx=f, dfx=df, xn=0, precision_required=10 ** -5))
# Gradient pour g(x)
print(gradient(fx=g, dfx=dg, xn=0, precision_required=10 ** -5))


def gradient_with_list_points(fx: Callable, dfx: Callable, xn: float = 0, precision_required: float = 10 ** -5,
                              max_iteration: int = 1000, step: float = 0.01) -> (
        Tuple)[float, float, int, List[List[float]]]:
    """
    Same function as gradient(...) but also return a list of all points the algorithm pass threw.

    :param fx: function f(x)
    :param dfx: the derivative of the function
    :param xn: the initial point of the function (x value)
    :param precision_required: the precision required to stop the algorithm (ex: 10**-5)
    :param max_iteration: max number of iteration before stopping the algorithm. default = 1000
    :param step: the step after each iteration. default = 0.01
    :return: A tuple containing the value of x, the result of the final f(x), the number of iteration and the list of points
    """
    derive: float = dfx(xn)
    iteration: int = 0
    list_points: List[List[float]] = [[xn, fx(xn)]]
    # If the absolute value of the derivative is inferior or equal to our required precision (epsilon) or if we reach
    # the max iteration we stop the program
    while abs(derive) > precision_required and iteration < max_iteration:
        derive = dfx(xn)
        xn = xn - (step * derive)
        iteration += 1
        list_points.append([xn, fx(xn)])
    return xn, fx(xn), iteration, list_points


# Gradient pour f(x)
x = np.linspace(start=0, stop=5.5)
_, _, _, result = gradient_with_list_points(fx=f, dfx=df, xn=0, step=0.1)
plt.plot(x, f(x), color="red")
for elem in result:
    plt.plot(elem[0], elem[1], marker="o", markeredgecolor="blue", markerfacecolor="black")
plt.grid()
plt.legend(["f(x)", "Avancement de la descente de gradient"])
plt.show()

# Gradient pour g(x)
x = np.linspace(start=-0.1, stop=5.5)
_, _, _, result = gradient_with_list_points(fx=g, dfx=dg, xn=0, step=0.1)
plt.plot(x, g(x), color="red")
for elem in result:
    plt.plot(elem[0], elem[1], marker="o", markeredgecolor="blue", markerfacecolor="black")
plt.grid()
plt.legend(["g(x)", "Avancement de la descente de gradient"])
plt.show()


def gradient_inertie(fx: Callable, dfx: Callable, xn: float = 0, precision_required: float = 10 ** -5,
                     max_iteration: int = 1000, step: float = 0.01, step_min: float = 0.001, step_max=0.1) -> (
        Tuple)[float, float, int, List[List[float]]]:
    """
    Same function as gradient(...) but also return a list of all points the algorithm pass threw.

    :param fx: function f(x)
    :param dfx: the derivative of the function
    :param xn: the initial point of the function (x value)
    :param precision_required: the precision required to stop the algorithm (ex: 10**-5)
    :param max_iteration: max number of iteration before stopping the algorithm. default = 1000
    :param step: the step after each iteration. default = 0.01
    :param step_min: the min step after each iteration. default = 0.01
    :param step_max: the max step after each iteration. default = 0.1
    :return: A tuple containing the value of x, the result of the final f(x), the number of iteration and the list of points
    """

    def update_step(derive_xn: float, xn_plus_1: float, step: float) -> float:
        """
        Update the step (alpha) of the gradient function, based on the max and min value defined for the step.

        @param derive_xn: the derivative value of xn
        @param xn_plus_1: the xn+1 value
        @param step: the actual step
        @return: the new step
        """
        derive_xn_plus_1: float = dfx(xn_plus_1)
        signe_xn = 0 if derive_xn < 0 else 1
        signe_xn_plus_1 = 0 if derive_xn_plus_1 < 0 else 1
        if signe_xn == signe_xn_plus_1:
            step *= 1.5
            if step > step_max:
                step = step_max
        else:
            step = step / 2
            if step < step_min:
                step = step_min
        return step

    derive: float = dfx(xn)
    iteration: int = 0
    list_points: List[List[float]] = [[xn, fx(xn)]]
    # If the absolute value of the derivative is inferior or equal to our required precision (epsilon) or if we reach
    # the max iteration we stop the program
    while abs(derive) > precision_required and iteration < max_iteration:
        derive = dfx(xn)
        xn = xn - (step * derive)
        step = update_step(derive_xn=derive, xn_plus_1=xn, step=step)
        iteration += 1
        list_points.append([xn, fx(xn)])
    return xn, fx(xn), iteration, list_points


# Gradient pour f(x)
x = np.linspace(start=0, stop=5.5)
_, _, _, result = gradient_inertie(fx=f, dfx=df, xn=5)
plt.plot(x, f(x), color="red")
for elem in result:
    plt.plot(elem[0], elem[1], marker="o", markeredgecolor="blue", markerfacecolor="black")
plt.grid()
plt.legend(["f(x)", "Avancement de la descente de gradient"])
plt.show()

# Gradient pour g(x)
x = np.linspace(start=-0.1, stop=5.5)
_, _, _, result = gradient_inertie(fx=g, dfx=dg, xn=1.5)
plt.plot(x, g(x), color="red")
for elem in result:
    plt.plot(elem[0], elem[1], marker="o", markeredgecolor="blue", markerfacecolor="black")
plt.grid()
plt.legend(["g(x)", "Avancement de la descente de gradient"])
plt.show()
